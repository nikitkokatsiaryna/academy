# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Teachers(models.Model):
    _name = 'academy.teachers'

    name = fields.Char()
    biography = fields.Html()
    photo = fields.Binary()
    age = fields.Integer()
    level = fields.Float()
    patient = fields.Boolean()
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], help='Gender'
    )
    staff_dob = fields.Date(string='Date of Birth')
    datetime = fields.Datetime(default=fields.Datetime.now())
    description = fields.Text()
    # salary = fields.Monitory()
    reference = fields.Reference(['product_template', 'string'])

    course_ids = fields.One2many('product.template', 'teacher_id', string="Courses")


class Courses(models.Model):
    _inherit = 'product.template'

    name = fields.Char()
    teacher_id = fields.Many2one('academy.teachers', string="Teacher")
